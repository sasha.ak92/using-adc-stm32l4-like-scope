import qbs

CppApplication {

    name: "QT-NUCLEO-L433RC-P"
    type: ["application", "bin", "hex"]

    Depends {
        name: "cpp"
    }



//    files: "main.cpp"
    consoleApplication: true
    cpp.positionIndependentCode: false
    cpp.executableSuffix: ".elf"

    cpp.cLanguageVersion: "c11"
    cpp.cxxLanguageVersion: "c++17"

    cpp.cFlags: "-std=c11"
    cpp.cxxFlags: "-std=c++17"

    cpp.includePaths: [ ]

    cpp.defines: [
        "USE_HAL_DRIVER",
        "STM32L4xx",
        "STM32L4",
//        "USE_STM32F429I_DISCO",
        "__weak=__attribute__((weak))",
        "__packed=__attribute__((__packed__))",
        "__FPU_PRESENT=1",
        "ARM_MATH_CM4",
        "ARCH_STM32",
//        "USE_USB_COMPOSITE",
//        "USE_HAL_TIM_REGISTER_CALLBACKS=1"
    ]

    cpp.commonCompilerFlags: [
        "-mcpu=cortex-m4",
        "-mfloat-abi=hard",
        "-mfpu=fpv4-sp-d16",
        "-mthumb",
        "-std=c++17",
        "-std=c11",
        "-u_printf_float",
    ]

    cpp.driverFlags: [
        "-specs=nosys.specs",
        "-specs=nano.specs",
        "-mcpu=cortex-m4",
        "-mfloat-abi=hard",
        "-mfpu=fpv4-sp-d16",
        "-mthumb",
        "-Xlinker",
        "--gc-sections",
        "-u_printf_float",
        "-Wl,-Map=" + path + "/Varna.map",
    ]

    cpp.linkerFlags:
        if((DebugVersionFlag == true) && (BootloaderFlag == false)) {
            [
                "--start-group",
                "-T" + path + "/STM32F429IITx_FLASH_DEBUG.ld",
                "--end-group"
            ]
        }
        else if((DebugVersionFlag == true) && (BootloaderFlag == true)) {
            [
                "--start-group",
                "-T" + path + "/STM32F429IITx_FLASH_BOOTLOADER.ld",
                "--end-group"
            ]
        }
        else {
            [
                "--start-group",
                "-T" + path + "/STM32F429IITx_FLASH_RELEASE_VERSION.ld",
                "--end-group"
            ]
        }

    cpp.libraryPaths:
        [
//            Home + "/Middlewares/STemWin/Lib",
//            STemWin + "/STemWin_Addons",
            Home + "/Drivers/CMSIS/Lib/GCC",
        ]

    cpp.staticLibraries:
        [
//            ":STemWin_CM4_OS_wc32.a",
        ]


    Properties {
        condition: qbs.buildVariant === "debug"
        cpp.debugInformation: true
        cpp.optimization: "none"
    }

    Properties {
        condition: qbs.buildVariant === "release"
        cpp.debugInformation: false
        cpp.optimization: "small"
    }

    Properties {
        condition: cpp.debugInformation
        cpp.defines: outer.concat("DEBUG")
    }

    Group {
        fileTagsFilter: product.type
        qbs.install: true
    }

    Rule {
        id: binDebugFrmw
        condition: qbs.buildVariant === "debug"
        inputs: ["application"]

        Artifact {
            fileTags: ["bin"]
            filePath: input.baseDir + "/" + input.baseName + ".bin"
        }

        prepare: {
            var objCopyPath = "arm-none-eabi-objcopy"
            var argsConv = ["-O", "binary", input.filePath, output.filePath]
            var cmd = new Command(objCopyPath, argsConv)
            cmd.description = "converting to BIN: " + FileInfo.fileName(
                        input.filePath) + " -> " + input.baseName + ".bin"

            var argsFlashingInternalFlash =
            [           "-f", "board/stm32f429disc1.cfg",
                        "-c", "init",
                        "-c", "reset init",
                        "-c", "flash write_image erase " + input.filePath,
                        "-c", "reset",
                        "-c", "shutdown"
            ]

            var cmdFlashInternalFlash = new Command("openocd", argsFlashingInternalFlash);
            cmdFlashInternalFlash.description = "Wrtie to the internal flash"

            return [cmd]
        }
    }

    Rule {
        id: binFrmw
        condition: qbs.buildVariant === "release"
        inputs: ["application"]

        Artifact {
            fileTags: ["bin"]
            filePath: input.baseDir + "/" + input.baseName + ".bin"
        }

        prepare: {
            var objCopyPath = "arm-none-eabi-objcopy"
            var argsConv = ["-O", "binary", input.filePath, output.filePath]
            var cmd = new Command(objCopyPath, argsConv)
            cmd.description = "converting to BIN: " + FileInfo.fileName(
                        input.filePath) + " -> " + input.baseName + ".bin"

            var argsFlashingInternalFlash =
            [           "-f", "board/stm32f429disc1.cfg",
                        "-c", "init",
                        "-c", "reset init",
                        "-c", "flash write_image erase " + input.filePath,
                        "-c", "reset",
                        "-c", "shutdown"
            ]

            var cmdFlashInternalFlash = new Command("openocd", argsFlashingInternalFlash);
            cmdFlashInternalFlash.description = "Wrtie to the internal flash"

            return [cmd]
        }
    }

    Rule {
        id: hexFrmw
        condition: qbs.buildVariant === "release"
        inputs: ["application"]

        Artifact {
            fileTags: ["hex"]
            filePath: input.baseDir + "/" + input.baseName + ".hex"
        }

        prepare: {
            var objCopyPath = "arm-none-eabi-objcopy"
            var argsConv = ["-O", "ihex", input.filePath, output.filePath]
            var cmd = new Command(objCopyPath, argsConv)
            cmd.description = "converting to HEX: " + FileInfo.fileName(
                        input.filePath) + " -> " + input.baseName + ".hex"

            return [cmd]
        }
    }

}
